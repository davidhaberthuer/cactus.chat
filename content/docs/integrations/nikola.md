---
title: "Nikola"
description: "Integrating Cactus Comments with Nikola."
lead: "Integrating Cactus Comments with the Nikola static-site generator."
date: 2020-10-17T12:44:00+02:00
lastmod: 2021-12-28T13:13:00+01:00
draft: false
images: []
menu:
  docs:
    parent: "integrations"
weight: 41
toc: true
---

If you are using [Nikola](https://getnikola.com) to generate your website, you can use [this plugin](https://plugins.getnikola.com/v8/cactuscomments/) to use Cactus Comments as a comment system.
It requires [Nikola v8.2.0](https://getnikola.com/blog/nikola-v820-is-out.html) or higher, where the feature to add a comments system through a plugin was added.

The plugin can be installed with
```bash
nikola plugin -i cactuscomments
```
A minimal configuration corresponding to the [quickstart guide](https://cactus.chat/docs/getting-started/quick-start/) would be
```python
COMMENT_SYSTEM = "cactus"
COMMENT_SYSTEM_ID = "<YOUR-SITE_NAME>"
GLOBAL_CONTEXT = {
    "cactus_config": {
        "defaultHomeserverUrl": "https://matrix.cactus.chat:8448",
        "serverName": "cactus.chat"
        }
    }
```
Other [configuration options](https://cactus.chat/docs/reference/web-client/#configuration) can also be added under `cactus_config` in `GLOBAL_CONTEXT`.

In case you are using an older Nikola version (up to v8.1.3) you should consider updating; if that is not an option [this script](https://github.com/pieterdavid/nikola-cactus-comments) may be used to change the comments system by modifying the templates of your theme.
